import App from './App.svelte';

const props = {
	colors: [ "white", "red", "green", "blue", "white" ],
	images: [
		'kari-shea-QfAX7_xjxm4-unsplash.jpg',
		'thor-alvis-sgrCLKYdw5g-unsplash.jpg',
		'rene-bohmer-YeUVDKZWSZ4-unsplash.jpg',
		'gian-d-jZxairpkhho-unsplash.jpg',
		'benjamin-wong-WoViiJWKLik-unsplash.jpg'
	]
};

[...document.getElementsByClassName('scroll-test')]
	.forEach(el => new App({ target: el, props: { ...props, content: el.children }, hydrate: true }));